//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Marcel Mravec on 15.01.2022.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
